# -*- coding: utf-8 -*-
# Démineur - Projet NSI Clotilde + Noah

#================ IMPORTER MODULES ================#
from tkinter import *
import random
from tkinter.font import Font
#==================================================#


#================== FONCTIONS ==================#


## Grilles:                           *  *  *  *  *  *  *  *  *  *  *  *  *  *

def créer_terrainMiné(clr):     
    global carrésAVérifier, tailleGrille
    X1=300
    Y1=100
    X2=X1+tailleGrille['avancée']
    Y2=Y1+tailleGrille['avancée']
    for i in range(tailleGrille['quantitécases']):
        n=i
        grille.append([])
        grille_marqueurs.append([])
        for j in range(tailleGrille['quantitécases']):
            rect=Canevas.create_rectangle(X1,Y1,X2,Y2,fill=clr)
            X1+=tailleGrille['avancée']
            X2+=tailleGrille['avancée']
            grille[n].append(0)
            grille_marqueurs[n].append(0)
        X1=300
        X2=X1+tailleGrille['avancée']
        Y1+=tailleGrille['avancée']
        Y2+=tailleGrille['avancée']

# Deuxième grille:
    
def créer_deuxiemeTerrain():   
    '''
    Créé une deuxième grille "au-dessus" de celle avec les bombes pour les cacher. J'ai peur de toucher les chiffres donc la liste grilleDeux a l'air un peu étrange... Tant que ça marche.
    '''
    global tailleGrille
    X1=300
    Y1=100
    X2=X1+tailleGrille['avancée']
    Y2=Y1+tailleGrille['avancée']
    for i in range(tailleGrille['quantitécases']):
        n=i
        grilleDeux.append([])
        for j in range(tailleGrille['quantitécases']):
            rect2=Canevas.create_rectangle(X1,Y1,X2,Y2,fill='#4f6e43')
            X1+=tailleGrille['avancée']
            X2+=tailleGrille['avancée']
            grilleDeux[n].append(rect2)
        X1=300
        X2=X1+tailleGrille['avancée']
        Y1+=tailleGrille['avancée']
        Y2+=tailleGrille['avancée']

# Grille Menu start:

def créer_terrainDéfaut(clr):
    '''
    Celle qui apparait avant que le jeu commence. Evite un souci avec les listes.
    '''
    global tailleGrille
    X1=300
    Y1=100
    X2=X1+tailleGrille['avancée']
    Y2=Y1+tailleGrille['avancée']
    for i in range(tailleGrille['quantitécases']):
        n=i
        for j in range(tailleGrille['quantitécases']):
            Canevas.create_rectangle(X1,Y1,X2,Y2,fill=clr)
            X1+=tailleGrille['avancée']
            X2+=tailleGrille['avancée']
        X1=300
        X2=X1+tailleGrille['avancée']
        Y1+=tailleGrille['avancée']
        Y2+=tailleGrille['avancée']


## Clic gauche, clic droit:          *  *  *  *  *  *  *  *  *  *  *  *  *  *

def clic_gauche(event):
    '''
    Ce qui se passe quand le joueur clique quelque part (dans la grille).
    Si il clique sur une bombe, il perd.
    Si il clique sur une case avec une/des bombe(/s) autour, la quantité de celles-ci apparaissent sur la case.
    Si il clique sur une case avec aucunes bombes autour, les cases aux alentours se révéleront jusqu'à atteindre une case avec des bombes autour, créant un "périmètre".
    Si c'est le premier clic de la partie, la fonction bombe sera aussi lancée.
    '''
    global premier_clic, carrésAVérifier, gameOver, tailleGrille
    x=event.x
    y=event.y
    if 300<x<900 and 100<y<700:
        col=(x-300)//tailleGrille['avancée']                          #// doc "Calcul placer une bombe dans la liste grille"
        rang=(y-100)//tailleGrille['avancée']
        if 0 <= col < tailleGrille['quantitécases'] and 0 <= rang < tailleGrille['quantitécases']:
            if premier_clic==True:
                créer_terrainMiné('#9c7c51')
                BOMBE(col,rang)
                premier_clic=False
                créer_deuxiemeTerrain()
            if grille_marqueurs[rang][col]==0 and gameOver!=True and grilleDeux[rang][col]!=0:
                if grille[rang][col]==1:
                    game_over(True)
                else:
                    nbbombes=combien_autour(rang,col)
                    rect2=grilleDeux[rang][col]
                    Canevas.delete(rect2)
                    grilleDeux[rang][col]=0 
                    if nbbombes==0:
                        carrésAVérifier-=1
                        périmètre(col,rang)
                    else:
                        afficher_nombre(col,rang,nbbombes)
                        carrésAVérifier-=1
    if carrésAVérifier<=0:
        game_over(True)
            

            
def clic_droit(event):
    '''
    Ce qu'il se passe quand le jouer fait un clic droit.
    Place le marqueur, ou l'enleve si il y en a déjà un là.
    '''
    global nb_marqueur, tailleGrille, marqueur
    x=event.x
    y=event.y
    if 300<x and x<900 and 100<y and y<700:
        col=(x-300)//tailleGrille['avancée']                        #// doc "Calcul placer une bombe dans la liste grille"
        rang=(y-100)//tailleGrille['avancée']
        if 0<=rang<len(grille_marqueurs) and 0<=col<len(grille_marqueurs[rang]) and grilleDeux[rang][col]!=0:
            if grille_marqueurs[rang][col]==0:
                mrq=Canevas.create_image(tailleGrille['milieuX']+col*tailleGrille['avancée'],tailleGrille['milieuY']+rang*tailleGrille['avancée'],image=marqueur)
                grille_marqueurs[rang][col]=2
                pos_marqueurs[(rang,col)]=mrq          #// associe chaque image avec sa position dans la grille
                nb_marqueur-=1
                Canevas.itemconfig(marqueursrestants, text="Marqueurs Restants : " + str(nb_marqueur))
            elif grille_marqueurs[rang][col]==2:
                mrq=pos_marqueurs.get((rang, col))
                Canevas.delete(mrq)
                grille_marqueurs[rang][col]=0
                nb_marqueur+=1
                Canevas.itemconfig(marqueursrestants, text="Marqueurs Restants : " + str(nb_marqueur))
    
    
    
## Sous-fonctions des clics:         *  *  *  *  *  *  *  *  *  *  *  *  *  *

def combien_autour(rang, col):
    '''
    Calcule combien il y a de bombes autour d'une case donnée.
    La première boucle 'if' permet de faire marcher la fonction même si la case est placé au bord de la grille.
    '''
    nb = 0  # nb de bombes
    for i in range(rang-1, rang+2):
        for j in range(col-1, col+2):
            if 0<=i<len(grille) and 0<=j<len(grille[0]):
                if grille[i][j]==1:
                    nb += 1
    return nb


def afficher_nombre(rang,col,nb_bombes):
    '''
    Affiche le nombre de bombes autour d'une case donnée.
    '''
    global tailleGrille
    police_chiffres=Font(family='Malgun gothic', size=tailleGrille['taillePolice'])
    if nb_bombes==1:
        Canevas.create_text(tailleGrille['milieuX']+rang*tailleGrille['avancée'],tailleGrille['milieuY']+col*tailleGrille['avancée'],text='1',fill='#153C87',font=police_chiffres)
    elif nb_bombes==2:
        Canevas.create_text(tailleGrille['milieuX']+rang*tailleGrille['avancée'],tailleGrille['milieuY']+col*tailleGrille['avancée'],text='2',fill='#00A10B',font=police_chiffres)
    elif nb_bombes==3:
        Canevas.create_text(tailleGrille['milieuX']+rang*tailleGrille['avancée'],tailleGrille['milieuY']+col*tailleGrille['avancée'],text='3',fill='#9F0000',font=police_chiffres)
    elif nb_bombes==4:
        Canevas.create_text(tailleGrille['milieuX']+rang*tailleGrille['avancée'],tailleGrille['milieuY']+col*tailleGrille['avancée'],text='4',fill='#2F28AA',font=police_chiffres)
    elif nb_bombes==5:
        Canevas.create_text(tailleGrille['milieuX']+rang*tailleGrille['avancée'],tailleGrille['milieuY']+col*tailleGrille['avancée'],text='5',fill='#F4471A',font=police_chiffres)
    elif nb_bombes==6:
        Canevas.create_text(tailleGrille['milieuX']+rang*tailleGrille['avancée'],tailleGrille['milieuY']+col*tailleGrille['avancée'],text='6',fill='#299279',font=police_chiffres)                        
    elif nb_bombes==7:
        Canevas.create_text(tailleGrille['milieuX']+rang*tailleGrille['avancée'],tailleGrille['milieuY']+col*tailleGrille['avancée'],text='7',fill='#343434',font=police_chiffres)                        
    elif nb_bombes==8:
        Canevas.create_text(tailleGrille['milieuX']+rang*tailleGrille['avancée'],tailleGrille['milieuY']+col*tailleGrille['avancée'],text='8',fill='#696969',font=police_chiffres)                                   
    
    
def BOMBE(col,rang):
    '''
    Place les bombes et les ajoute dans la liste grille en tant que "1".
    Boucle True empêche qu'elles soient placées dans la même case et que le premier clic tombe sur une bombe.
    '''
    global milieuX, milieuY
    col_premierclic=col
    rang_premierclic=rang
    for i in range(qttébombes):
        while True:
            coord=random.choice(milieuX)
            coord2=random.choice(milieuY)
            if [coord, coord2] not in coordo and col_premierclic!=(coord-300)//tailleGrille['avancée'] and rang_premierclic!=(coord2-100)//tailleGrille['avancée']: # 😀🔫
                break
        coordo.append([coord, coord2]) # regroupe les coordonees ensemble 
        rang=int((coord2-100)//tailleGrille['avancée'])
        col=int((coord-300)//tailleGrille['avancée'])
        grille[rang][col]=1                              
        Canevas.create_oval(coord-tailleGrille['tailleBombe'],coord2-tailleGrille['tailleBombe'],coord+tailleGrille['tailleBombe'],coord2+tailleGrille['tailleBombe'],fill='pink')

def périmètre(col,rang):
    '''
    Si le joueur clique sur une case qui rend un "0" après être passée dans combien_autour, elle et toutes les cases similaires qui la touche se révélent jusqu'a créer un 'périmètre' de chiffre.
    '''
    global carrésAVérifier, nb_marqueur
    for i in range(rang-1, rang+2):
        for j in range(col-1,col+2):
            if 0<=i<len(grille) and 0<=j<len(grille[0]) and grilleDeux[i][j]!=0:  #😐
                nbbombes=combien_autour(i, j)
                if nbbombes==0:
                    rect=grilleDeux[i][j]
                    Canevas.delete(rect)
                    if grille_marqueurs[rang][col]==2:
                        mrq=pos_marqueurs.get((rang, col))
                        Canevas.delete(mrq)
                        grille_marqueurs[rang][col]=0
                        nb_marqueur+=1
                        Canevas.itemconfig(marqueursrestants, text="Marqueurs Restants : " + str(nb_marqueur))
                    grilleDeux[i][j]=0
                    carrésAVérifier-=1
                    périmètre(j, i)
                    
                else:
                    rect=grilleDeux[i][j]
                    if grille_marqueurs[i][j]==2:
                        mrq=pos_marqueurs.get((i, j))
                        Canevas.delete(mrq)
                        grille_marqueurs[i][j]=0
                        nb_marqueur+=1
                        Canevas.itemconfig(marqueursrestants, text="Marqueurs Restants : " + str(nb_marqueur))
                    Canevas.delete(rect)
                    grilleDeux[i][j]=0
                    afficher_nombre(j,i,nbbombes)
                    carrésAVérifier-=1


## Interface:                        *  *  *  *  *  *  *  *  *  *  *  *  *  *
        
def game_over(over):
    '''
    Affiche l'écran 'game over'.
    Contient le bouton 'Restart' et le bouton 'Menu'.
    Affiche score.
    '''
    global texteGameOver, img, carrésAVérifier
    if over==True :
        if carrésAVérifier<=0:
            img=Canevas.create_image(600,420, image=gagnant)
            texteGameOver=Canevas.create_text(600,50,text='Gagné! Veux-tu recommencer?',fill='#000000',font=police_gameover)

        else:
            #détruire la deuxieme grille ?
            img=Canevas.create_image(600,420,image=perdant)
            texteGameOver=Canevas.create_text(600,50,text='Perdu, recommence!',fill='#000000',font=police_gameover)
       
        clic_interdit()
                  
    
def restart():
    '''
    Lance une nouvelle partie - efface les grilles, réinitialise les listes...
    '''
    global grille, grilleDeux, grille_marqueurs, premier_clic, t, nb_marqueur, gameOver, texteGameOver, img, carrésAVérifier, code, coordo, tailleGrille, qttébombes
    grille=[]
    grilleDeux=[]
    grille_marqueurs=[]
    coordo=[]
    premier_clic=True
    t=0
    nb_marqueur=qttébombes
    gameOver=False
    carrésAVérifier=tailleGrille['quantitécases']*tailleGrille['quantitécases']-qttébombes
    code=''
    Canevas.delete(all)
    Canevas.delete(texteGameOver)
    Canevas.delete(img)
    créer_terrainDéfaut('#4f6e43')
    Canevas.itemconfig(marqueursrestants, text="Marqueurs Restants : " + str(nb_marqueur))
    clic_possible()

def choix_bombes():
    global qttébombes, nb_marqueur, avertissement, carrésAVérifier
    nb=Choix.get()
    if int(nb)<tailleGrille['quantitécases']*tailleGrille['quantitécases']-1:
        qttébombes=int(nb)
        nb_marqueur=int(nb)
        carrésAVérifier=tailleGrille['quantitécases']*tailleGrille['quantitécases']-qttébombes
        restart()
    
    

## Travaille en arrière-plan         *  *  *  *  *  *  *  *  *  *  *  *  *  *

def coord_case(liste, ind=0):
    '''
    Ajoute toutes les coordonnées de chaque case dans une liste. 
    A retravailler pour qu'elle le fasse pour les cases égales Ã  1.
    '''
    for i in range(30):   #parce que 30 bombes... 
        HG=[liste[ind][0]-25, liste[ind][1]-25]   # coord du coin haut-gauche
        HD=[liste[ind][0]+25, liste[ind][1]-25]   # // haut-droite
        BG=[liste[ind][0]-25, liste[ind][1]+25]   # // bas-gauche
        BD=[liste[ind][0]+25, liste[ind][1]+25]   # // bas-droite
        cases_bombes.append([HG, HD, BG, BD])
        ind+=1
        
def clic_possible():
    Canevas.bind('<Button-1>', clic_gauche)
    Canevas.bind('<Button-3>', clic_droit)

def clic_interdit():
    Canevas.unbind('<Button-1>')
    Canevas.unbind('<Button-3>')
    
    
## Difficultés                             *  *  *  *  *  *  *  *  *  *  *  *
def petite_grille():
    global milieuX, milieuY, carrésAVérifier, tailleGrille, qttébombes, marqueur
    tailleGrille=PetiteGrille
    milieuX=[350,450,550,650,750,850]
    milieuY=[150,250,350,450,550,650]
    marqueur=PhotoImage(file='marqueurpetit.gif')
    carrésAVérifier=tailleGrille['quantitécases']*tailleGrille['quantitécases']-qttébombes
    restart()
    
def moyenne_grille():
    global milieuX, milieuY, tailleGrille, carrésAVérifier, qttébombes, marqueur
    tailleGrille=MoyenneGrille
    milieuX=[325,375,425,475,525,575,625,675,725,775,825,875]
    milieuY=[125,175,225,275,325,375,425,475,525,575,625,675]
    marqueur=PhotoImage(file='marqueurmoy.gif')
    carrésAVérifier=tailleGrille['quantitécases']*tailleGrille['quantitécases']-qttébombes
    restart()
    
def grande_grille():
    global milieuX, milieuY, tailleGrille, carrésAVérifier, qttébombes, marqueur
    tailleGrille=GrandeGrille
    milieuX=[300+25/2,325+25/2,350+25/2,375+25/2,400+25/2,425+25/2,450+25/2,475+25/2,500+25/2,525+25/2,550+25/2,575+25/2,600+25/2,625+25/2,650+25/2,675+25/2,700+25/2,725+25/2,750+25/2,775+25/2,800+25/2,825+25/2,850+25/2,875+25/2]
    milieuY=[100+25/2,125+25/2,150+25/2,175+25/2,200+25/2,225+25/2,250+25/2,275+25/2,300+25/2,325+25/2,350+25/2,375+25/2,400+25/2,425+25/2,450+25/2,475+25/2,500+25/2,525+25/2,550+25/2,575+25/2,600+25/2,625+25/2,650+25/2,675+25/2,]
    marqueur=PhotoImage(file='marqueurgrand.gif')
    carrésAVérifier=tailleGrille['quantitécases']*tailleGrille['quantitécases']-qttébombes
    restart()
        
## Debug                             *  *  *  *  *  *  *  *  *  *  *  *  *  *

def DEBUG(event):
    '''
    Marche seulement si le choix des bombes est ôté - plus trop utile.
    '''
    global code, carrésAVérifier
    lettre=event.char
    code+=lettre
    if code=="gagner":
        carrésAVérifier=0
        game_over(True)
    if code=="perdre":
        carrésAVérifier=99999999999
        game_over(True)
    if code=="montrerbombes":
        for i in grilleDeux:
            for rect in i:
                if rect!=0:
                    Canevas.delete(rect)

#================ FIN FONCTIONS ================#


#================== VARIABLES ==================#
grille=[] # Version liste de la grille. 1=bombe.
grille_marqueurs=[]
grilleDeux=[] # Version liste de la deuxième grille qui permet d'effacer les rectangles
GrandeGrille={'avancée':25, 'quantitécases':24, 'taillePolice':10, 'milieuX':300+25/2, 'milieuY':100+25/2, 'tailleBombe':5}
MoyenneGrille={'avancée':50, 'quantitécases':12, 'taillePolice':15, 'milieuX':325, 'milieuY':125, 'tailleBombe':10}
PetiteGrille={'avancée':100, 'quantitécases':6, 'taillePolice':30, 'milieuX':350, 'milieuY':150, 'tailleBombe':20}

tailleGrille={'avancée':50, 'quantitécases':12, 'taillePolice':15, 'milieuX':325, 'milieuY':125, 'tailleBombe':10}  # sera un choix - petit/moyen/large. Réduit ou agrandit la taille des cases.

code=''
qttébombes=25

# Fonctions clics:
premier_clic=True 
pos_marqueurs={}

# Fonction 'BOMBES':
milieuX=[325,375,425,475,525,575,625,675,725,775,825,875]
milieuY=[125,175,225,275,325,375,425,475,525,575,625,675]
coordo=[]

# Fonction 'coord_case':
cases_bombes=[]

# Interface:
nb_marqueur=qttébombes
texteGameOver=""
img=''

# Jeu en lui-même:
carrésAVérifier=tailleGrille['quantitécases']*tailleGrille['quantitécases']-qttébombes
gameOver=False



#================ FIN VARIABLES ================#


#================== INTERFACE GRAPHIQUE ==================#
# Fenetre:
FenetreJeu=Tk()
FenetreJeu.title("Démineur AUGMENTÉ!")
Canevas=Canvas(FenetreJeu,width=1200,height=800,bg ='grey')
Canevas.pack()
Canevas.focus_set()

# Polices (texte):
police_gameover = Font(family='Liberation Serif', size=45)
police_marqueursrestants = Font(family='Liberation Serif', size=15)
police_chiffres=Font(family='Malgun gothic', size=tailleGrille['taillePolice'])
police_avertissement=Font(family='Liberation Serif', size=20)
police_autre=Font(family='Liberation Serif', size=12)
police_bouton=Font(family='Liberation Serif', size=17)
police_titre=Font(family='Liberation Serif', size=21)

# Images:
perdant=PhotoImage(file='partieperdue.png')
marqueur=PhotoImage(file='marqueurmoy.gif')
gagnant=PhotoImage(file='partiegagnée.png')

# Arrière-plan/Autour de la grille:
marqueursrestants=Canevas.create_text(150,405,text="Marqueurs Restants : " + str(nb_marqueur),fill='#000000',font=police_marqueursrestants)
bouton_restart=Button(FenetreJeu, text="Nouvelle partie", command=restart, height=2, width=15, font=police_bouton)
bouton_restart.place(x=50, y=315)
bouton_grilleP=Button(FenetreJeu, text="Petite Grille", command=petite_grille, width=20)
bouton_grilleP.place(x=975, y=250)
bouton_grilleM=Button(FenetreJeu, text="Moyenne Grille", command=moyenne_grille, width=20)
bouton_grilleM.place(x=975, y=300)
bouton_grilleG=Button(FenetreJeu, text="Grande Grille", command=grande_grille, width=20)
bouton_grilleG.place(x=975, y=350)
Choix=Entry(FenetreJeu,bg='cornsilk2',fg='dark slate grey', font=('Liberation 12'), width=300)
Choix.place(x=985, y=400, height=60, width=60)
entrerchoix=Button(FenetreJeu, text='OK', command=choix_bombes)
entrerchoix.place(x=1060, y=415, height=35, width=35)
conseil=Canevas.create_text(145,430,text="Clic droit pour placer un marqueur!",fill='#474747',font=police_autre)
conseilbmb=Canevas.create_text(1040,480,text="Nombre de bombes?",fill='#474747',font=police_autre)
conseilbmb2=Canevas.create_text(1050,500,text="Attention! Pas plus qu'il n'y a de cases!",fill='#474747',font=police_autre)
derrieretitre=Canevas.create_rectangle(10,210,290,300,fill="wheat3")
titre=Canevas.create_text(150,240,text='Démineur',fill="black",font=police_titre)
titre=Canevas.create_text(150,270,text='AUGMENTÉ',fill="black",font=police_titre)

# Grille:
Canevas.create_rectangle(300,100,900,700,fill='#4f6e43')
créer_terrainDéfaut('#4f6e43')

# Debug:
commande=Entry(FenetreJeu, bg='grey', fg='black')
commande.focus_set()
#================ FIN INTERFACE GRAPHIQUE ================#


#================== RECEPTIONNAIRE D'EVENEMENTS ==================#
Canevas.bind('<Button-1>',clic_gauche)
Canevas.bind('<Button-3>',clic_droit)
FenetreJeu.bind('<KeyPress>', DEBUG)
#================ FIN RECEPTIONNAIRE D'EVENEMENTS ================#


#================== PROGRAMME PRINCIPAL ==================#

#================ FIN PROGRAMME PRINCIPAL ================#


#================ FIN PROGRAMME ENTIER ================# 
FenetreJeu.mainloop()
