﻿Comment jouer au démineur ?
Pour gagner, il faut localiser les bombes cachées dans la grille représentant un champ de mines, avec pour seule indication le nombre de bombes dans les zones adjacentes.
Les marqueurs servent à protéger les cases de vos clic accidentels, il y a le même nombre de marqueurs que de bombes.

1-Choisir la taille de la grille avec les boutons à droite.
2-Définir le nombre de bombes cachées (le nombre de bombes par défaut étant de 25) puis appuyer sur le bouton OK.
3-La partie commence, pour déminer une des cases de la grille il suffit de faire clic gauche et pour mettre un marqueur sur une des cases que vous suspectez cacher une bombe faites clic droit. 
4-Une fois que vous avez gagné ou perdu appuyer sur le bouton "Nouvelle partie" (vous pouvez changer de taille de grille ou de nombre de bombes avant de recommencer la partie).